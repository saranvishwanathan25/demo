from django.shortcuts import render,redirect
from django.http import HttpResponse
from myapp.models import Empdetail
from myapp.forms import Empform


# Create your views here.
def create(request):
    form = Empform()
    if request.method=='POST':
        form=Empform(request.POST)
        if form.is_valid():
            form.save()
            #return HttpResponse('Details added to database')
            return redirect('/read')
    return render(request, 'create.html', {'forms':form})

def read(request):
    empdetails=Empdetail.objects.all()
    return render(request,'db.html',{'details':empdetails})

def delete(request,id):
    employee =Empdetail.objects.get(id=id)
    employee.delete()
    return redirect('/read')

def update(request,id):
    employee=Empdetail.objects.get(id=id)
    form=Empform(instance=employee)
    if request.method=='POST':
        form=Empform(request.POST, instance=employee)
        if form.is_valid():
            form.save()
            return redirect('/read')    
    else:
        return render(request,'update.html',{'forms':form})
